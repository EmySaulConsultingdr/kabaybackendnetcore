# PermissionBackend

Desarrollo de un WebApi para prueba de khensys
# Pasos para correr el proyecto PermissionBackend
1. Abrir proyecto
2. Setear proyecto PermissionControl por Defecto
3. Cambiar ConnectionStrings
    1. server
4. Ir a Package Manage Console
    1. Ejecutar comando "Update-Database" (Creara Base de datos y correrar el initial Seed *Insertando los Permission Types*)
5. Correr proyecto (Abrirá Swagger)



## Estructura del Projecto
![Alt text](/Docs/images/ProjectStructure.JPG "Estructura del Projecto")

## Dependencies: 
- Microsoft.AspNetCore.Mvc.NewtonsoftJson 3.1.8
- AutoMapper 10.1.1
- Microsoft.EntityFrameworkCore 5.0.0
- Microsoft.EntityFrameworkCore.Design 5.0.0
- Microsoft.EntityFrameworkCore.SqlServer
- Microsoft.EntityFrameworkCore.SqlServer 5.0.0
- Microsoft.EntityFrameworkCore.Tools 5.0.0
- Microsoft.Extensions.DependencyInjection 5.0.0
- NLog 4.7.5
- NUnit 3.12.0
- Shouldly 4.0.1
- Swashbuckle.AspNetCore 5.6.3

