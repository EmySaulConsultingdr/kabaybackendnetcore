﻿using Microsoft.EntityFrameworkCore;

namespace DatabaseContracts
{
    public interface DbContextContract
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();
    }
}
