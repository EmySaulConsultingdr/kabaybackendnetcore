﻿using DatabaseContracts;
using Domain.Data;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MSSQLServer.Repositories
{
    public class MSSQLBaseRepository<TEntity> : Domain.Repositories.BaseRepository<TEntity> where TEntity : Entity
    {
        protected DbContextContract _dbContextContract { get; set; }
        public MSSQLBaseRepository(DbContextContract dbContextContract)
        {
            this._dbContextContract = dbContextContract;
        }

        public Task<IQueryable<TEntity>> FindAllAsync(Expression<Func<TEntity, object>> include = null)
        {
            try
            {
                return Task.FromResult((include == null ? this._dbContextContract.Set<TEntity>().AsNoTracking()
                                : this._dbContextContract.Set<TEntity>().AsNoTracking().Include(include)).Where(x => !x.IsDeleted));
            }
            catch (Exception ex)
            {
                throw new CustomException(Domain.Constants.ERR_READING, $"Error obteniendo los datos. Revisar logs para más información", ex);
            }
        }
        public async Task<IQueryable<TEntity>> FindByConditionAsync(Expression<Func<TEntity, bool>> expression)
        {
            var result = await FindAllAsync();

            return result.Where(expression).AsNoTracking().Where(x => !x.IsDeleted);
        }

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            try
            {
                var entityAdded = await this._dbContextContract.Set<TEntity>().AddAsync(entity);
                this._dbContextContract.SaveChanges();
                return entityAdded.Entity;
            }
            catch (Exception ex)
            {
                throw new CustomException(Domain.Constants.ERR_CREATING, $"Error creando el registro. Revisar logs para más información", ex);
            }
        }

        public Task UpdateAsync(TEntity entity)
        {
            try
            {
                return Task.Run(() =>
                {
                    entity.LastModificationTime = new DateTime();
                    this._dbContextContract.Set<TEntity>().Update(entity);
                    this._dbContextContract.SaveChanges();
                });
            }
            catch (Exception ex)
            {
                throw new CustomException(Domain.Constants.ERR_UPDATING, "Error actualizando los datos. Revisar logs para más información", ex);
            }
        }

        public async Task DeleteAsync(int entityId)
        {
            var result = await this.FindAllAsync();
            var entity = await result.FirstOrDefaultAsync(x => x.Id == entityId);

            if (entity == null)
                throw new CustomException(Domain.Constants.ERR_NOT_FOUND, "No se pudo encontrar el registro para ser eliminado. Favor comuniquese con su administrador.");

            entity.IsDeleted = true;
            await this.UpdateAsync(entity);
        }

        public async Task<TEntity> FindAsync(int id, Expression<Func<TEntity, object>> include = null)
        {
            var entity = await FindAsync(x => x.Id == id, include);

            if (entity == null)
                throw new CustomException(Domain.Constants.ERR_NOT_FOUND, $"No se pudo encontrar el registro con el id {id}. Favor comuniquese con su administrador.");

            return entity;
        }

        public async Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> expression, Expression<Func<TEntity, object>> include = null)
        {
            var result = await this.FindByConditionAsync(expression);
            if (include != null)
                result = result.Include(include);

            return result.FirstOrDefault();
        }
    }
}
