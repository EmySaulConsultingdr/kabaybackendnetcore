﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DomainBuiltIn
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateDynamicMap();
        }

        private void CreateDynamicMap()
        {
            List<Type> addedTypes = new List<Type>();

            var modelsToMap = typeof(Domain.Attributes.AutoMapAttribute).Assembly.GetTypes()
                                        .Where(x => x.GetCustomAttributes(typeof(Domain.Attributes.AutoMapAttribute), true).Any())
                                        .Select(x => new
                                        {
                                            source = x,
                                            target = (Type)x.GetCustomAttributesData()
                                                                      .SelectMany(s => s.ConstructorArguments)
                                                                      .FirstOrDefault().Value
                                        });
            foreach (var item in modelsToMap)
            {
                if (addedTypes.Contains(item.source) || addedTypes.Contains(item.target))
                    continue;

                CreateMap(item.source, item.target);
                CreateMap(item.target, item.source);
            }
        }
    }
}
