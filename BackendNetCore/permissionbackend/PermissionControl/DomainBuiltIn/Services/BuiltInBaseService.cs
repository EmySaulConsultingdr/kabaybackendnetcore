﻿using AutoMapper;
using Domain.Entities;
using System.Threading.Tasks;

namespace DomainBuiltIn.Services
{
    public class BuiltInBaseService<TEntity, TModel> : Domain.Services.BaseService<TEntity, TModel> where TEntity : Entity where TModel : class
    {
        public readonly Domain.Repositories.BaseRepository<TEntity> _entityRepository;
        public readonly IMapper _mapper;

        public BuiltInBaseService(Domain.Repositories.BaseRepository<TEntity> entityRepository,
            IMapper mapper)
        {
            _entityRepository = entityRepository;
            _mapper = mapper;
        }
        public virtual async Task<int> CreateAsync(TModel model)
        {
            var entity = _mapper.Map<TEntity>(model);
            var entityCreated = await _entityRepository.CreateAsync(entity);
            return entityCreated.Id;
        }

        public virtual Task DeleteAsync(int id)
        {
            return _entityRepository.DeleteAsync(id);
        }

        public virtual Task UpdateAsync(int id, TModel model)
        {
            var entity = _mapper.Map<TEntity>(model);
            entity.Id = id;

            return _entityRepository.UpdateAsync(entity);
        }
    }
}
