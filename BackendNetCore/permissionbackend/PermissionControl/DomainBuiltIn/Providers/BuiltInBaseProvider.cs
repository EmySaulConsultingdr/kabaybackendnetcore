﻿using AutoMapper;
using Domain.Entities;
using Domain.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DomainBuiltIn.Providers
{
    public class BuiltInBaseProvider<TEntity, TModel> : BaseProvider<TEntity, TModel> where TEntity : Entity where TModel : class
    {
        public readonly Domain.Repositories.BaseRepository<TEntity> _baseRepository;
        public readonly IMapper _mapper;
        public BuiltInBaseProvider(Domain.Repositories.BaseRepository<TEntity> baseRepository, IMapper mapper)
        {
            _baseRepository = baseRepository;
            _mapper = mapper;
        }
        public virtual async Task<TModel> GetAsync(int id)
        {
            var model = await _baseRepository.FindAsync(id);
            return _mapper.Map<TModel>(model);
        }

        public virtual async Task<IEnumerable<TModel>> GetAllAsync()
        {
            var result = await _baseRepository.FindAllAsync();
            return _mapper.Map<List<TModel>>(result.ToList());
        }

        public virtual async Task<IEnumerable<TModel>> GetAllAsync(Expression<Func<TEntity, bool>> expression)
        {
            var result = await _baseRepository.FindByConditionAsync(expression);
            return _mapper.Map<List<TModel>>(result.ToList());
        }
    }
}
