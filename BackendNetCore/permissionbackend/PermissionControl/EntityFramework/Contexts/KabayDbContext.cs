﻿using DatabaseContracts;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace EntityFramework.Contexts
{
    public class KabayDbContext : DbContext, DbContextContract
    {
        public KabayDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        public DbSet<CategoryAttribute> CategoryAttributes { get; set; }
        public DbSet<CategoryRoom> CategoryRooms { get; set; }
        public DbSet<CategoryRoomPhoto> CategoryRoomPhotos { get; set; }
        public DbSet<Local> Locales { get; set; }
        public DbSet<LocalPhoto> LocalPhotos { get; set; }
        public DbSet<Room> Rooms { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
