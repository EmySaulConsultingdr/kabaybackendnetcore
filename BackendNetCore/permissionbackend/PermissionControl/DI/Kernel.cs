﻿using AutoMapper;
using DatabaseContracts;
using Domain.Repositories;
using DomainBuiltIn;
using EntityFramework;
using EntityFramework.Contexts;
using MSSQLServer.Repositories;
using PermissionMock.Repositories;
using PermissionMock.Services;

namespace DI
{
    public class Kernel
    {
        private static Kernel _kernel = new Kernel();
        private Kernel() { }

        public static Kernel Instance { get { return _kernel; } }

        public void Setup(KernelRegistrar kernelRegistrar)
        {
            kernelRegistrar.RegisterAsSingleton<Domain.Services.LoggerService, DomainBuiltIn.Services.LoggerService>();
            kernelRegistrar.RegisterAsDbContext<DbContextContract, KabayDbContext>(EFConstants.DbContextName);
            kernelRegistrar.RegisterAsScope(typeof(BaseRepository<>), typeof(MSSQLBaseRepository<>));

            this.BasicSetup(kernelRegistrar);
        }
        public void TestSetup(KernelRegistrar kernelRegistrar)
        {
            kernelRegistrar.RegisterAsSingleton<Domain.Services.LoggerService, MockLogService>();
            kernelRegistrar.RegisterAsScope<BaseRepository<Domain.Entities.Local>, MockPermissionRepository>();
            this.BasicSetup(kernelRegistrar);
        }

        private void BasicSetup(KernelRegistrar kernelRegistrar)
        {
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            kernelRegistrar.RegisterAsSingleton(mapper);

            kernelRegistrar.RegisterAsScope(typeof(Domain.Services.BaseService<,>), typeof(DomainBuiltIn.Services.BuiltInBaseService<,>));
            kernelRegistrar.RegisterAsScope(typeof(Domain.Providers.BaseProvider<,>), typeof(DomainBuiltIn.Providers.BuiltInBaseProvider<,>));
        }

    }
}
