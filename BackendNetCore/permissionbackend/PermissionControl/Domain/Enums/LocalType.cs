﻿using System.ComponentModel;

namespace Domain.Enums
{
    public enum LocalTypeEnum : int
    {
        [Description("Cabaña")]
        Hovel = 1,
        [Description("Villa")]
        Villa = 2
    }
}
