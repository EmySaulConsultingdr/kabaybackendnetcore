﻿using System.ComponentModel;

namespace Domain.Enums
{
    public enum StatusRoomEnum : int
    {
        [Description("Libre")]
        Free = 1,
        [Description("Reservada")]
        Reservated = 2,
        [Description("En Espera")]
        InWait = 3
    }
}
