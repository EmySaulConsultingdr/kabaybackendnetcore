﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Providers
{
    public interface BaseProvider<TEntity, TModel> where TEntity : Entity where TModel : class
    {
        Task<TModel> GetAsync(int id);
        Task<IEnumerable<TModel>> GetAllAsync();
        Task<IEnumerable<TModel>> GetAllAsync(Expression<Func<TEntity, bool>> expression);

    }
}
