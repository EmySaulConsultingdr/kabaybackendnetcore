﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models
{
    [Attributes.AutoMap(typeof(Entities.LocalPhoto))]
    public class LocalPhoto
    {
        public int? Id { get; set; }
        public string Description { get; set; }
        public string Base64 { get; set; }
        public int LocalId { get; set; }
    }
}
