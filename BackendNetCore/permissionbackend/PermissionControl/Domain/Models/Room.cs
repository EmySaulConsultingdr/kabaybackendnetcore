﻿using Domain.Enums;
using System.Collections.Generic;
namespace Domain.Models
{
    [Attributes.AutoMap(typeof(Entities.Room))]
    public class Room
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int CategoryRoomId { get; set; }
        public StatusRoomEnum StatusRoom { get; set; }

    }
}
