﻿using Domain.Attributes;
using Domain.Enums;

namespace Domain.Models
{
    [AutoMap(typeof(Domain.Entities.Local))]
    public class Local
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string Owner { get; set; }
        public string Phone { get; set; }
        public LocalTypeEnum LocalType { get; set; }
        public string MainPhoto { get; set; }
    }
}
