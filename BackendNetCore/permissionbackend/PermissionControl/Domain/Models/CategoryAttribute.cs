﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    [Attributes.AutoMap(typeof(Entities.CategoryAttribute))]
    public class CategoryAttribute 
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; } = string.Empty;
        public int CategoryRoomId { get; set; }

    }
}
