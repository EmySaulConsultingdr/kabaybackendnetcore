﻿using System.Collections.Generic;

namespace Domain.Models
{
    [Attributes.AutoMap(typeof(Entities.CategoryRoom))]
    public class CategoryRoom
    {
        public int? Id { get; set; }
        public string Description { get; set; }
        public string MainPhoto { get; set; }
        public int LocalId { get; set; }
    }
}
