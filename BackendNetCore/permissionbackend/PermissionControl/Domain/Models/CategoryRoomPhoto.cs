﻿namespace Domain.Models
{
    [Attributes.AutoMap(typeof(Entities.CategoryRoomPhoto))]
    public class CategoryRoomPhoto
    {
        public int? Id { get; set; }
        public string Description { get; set; }
        public string Base64 { get; set; }
        public int CategoryRoomId { get; set; }
    }
}
