﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Local : Entity
    {

        public Local()
        {
            AdditionalPhotos = new HashSet<LocalPhoto>();
            CategoryRooms = new HashSet<CategoryRoom>();
        }

        public string Name { get; set; }
        public string Address { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string Owner { get; set; }
        public string Phone { get; set; }
        public LocalTypeEnum LocalType { get; set; }
        public string MainPhoto { get; set; }

        public virtual ICollection<LocalPhoto> AdditionalPhotos { get; set; }
        public virtual ICollection<CategoryRoom> CategoryRooms { get; set; }


    }
}
