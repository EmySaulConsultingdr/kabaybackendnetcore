﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class CategoryAttribute : Entity
    {
        public string Name { get; set; }
        public string Value { get; set; } = string.Empty;
        public int CategoryRoomId { get; set; }


        [ForeignKey(nameof(CategoryRoomId))]
        public virtual CategoryRoom Category { get; set; }
    }
}
