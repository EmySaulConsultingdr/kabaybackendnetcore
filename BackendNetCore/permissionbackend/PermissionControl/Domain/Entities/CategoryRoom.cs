﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class CategoryRoom : Entity
    {
        public CategoryRoom()
        {
            Rooms = new HashSet<Room>();
            AdditionalPhotos = new HashSet<CategoryRoomPhoto>();
            CategoryAttributes = new HashSet<CategoryAttribute>();
        }
        public string Description { get; set; }
        public string MainPhoto { get; set; }
        public int LocalId { get; set; }


        [ForeignKey(nameof(LocalId))]
        public virtual Local Local { get; set; }
        public virtual ICollection<CategoryAttribute> CategoryAttributes { get; set; }
        public virtual ICollection<Room> Rooms { get; set; }
        public virtual ICollection<CategoryRoomPhoto> AdditionalPhotos { get; set; }

    }
}
