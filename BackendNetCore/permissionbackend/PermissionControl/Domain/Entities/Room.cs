﻿using Domain.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Room : Entity
    {
        public string Name { get; set; }
        public int CategoryRoomId { get; set; }
        public StatusRoomEnum StatusRoom { get; set; }

        [ForeignKey(nameof(CategoryRoomId))]
        public virtual CategoryRoom CategoryRoom { get; set; }

    }
}
