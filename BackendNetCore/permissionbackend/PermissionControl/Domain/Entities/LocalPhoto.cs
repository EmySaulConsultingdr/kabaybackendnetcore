﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class LocalPhoto : Entity
    {
        public string Description { get; set; }
        public string Base64 { get; set; }
        public int LocalId { get; set; }


        [ForeignKey(nameof(LocalId))]
        public virtual Local Local { get; set; }
    }
}
