﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class CategoryRoomPhoto : Entity
    {
        public string Description { get; set; }
        public string Base64 { get; set; }
        public int CategoryRoomId { get; set; }


        [ForeignKey(nameof(CategoryRoomId))]
        public virtual CategoryRoom CategoryRoom { get; set; }
    }
}
