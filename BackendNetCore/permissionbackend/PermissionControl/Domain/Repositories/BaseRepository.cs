﻿using Domain.Entities;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public interface BaseRepository<TEntity> where TEntity : Entity
    {
        Task<TEntity> FindAsync(int id, Expression<Func<TEntity, object>> include = null);
        Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> expression, Expression<Func<TEntity, object>> include = null);
        Task<IQueryable<TEntity>> FindAllAsync(Expression<Func<TEntity, object>> include = null);
        Task<IQueryable<TEntity>> FindByConditionAsync(Expression<Func<TEntity, bool>> expression);
        Task<TEntity> CreateAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(int entityId);
    }
}
