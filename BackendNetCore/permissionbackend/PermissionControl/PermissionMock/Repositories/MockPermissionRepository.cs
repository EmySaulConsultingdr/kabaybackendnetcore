﻿using Domain.Data;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PermissionMock.Repositories
{
    public class MockPermissionRepository : Domain.Repositories.BaseRepository<Domain.Entities.Local>
    {
        private List<Local> permissions = new List<Local>();
        public MockPermissionRepository()
        {
            this.permissions.AddRange(new List<Local>() {
                new Local
                {
                    Id = 1
                   
                },
                new Local
                {
                    Id = 2
                  
                },
             });
        }
        private static int countId = 3;
        public Task<Local> CreateAsync(Local entity)
        {
            entity.Id = countId;
            this.permissions.Add(entity);
            countId++;
            return Task.FromResult(entity);
        }

        public Task DeleteAsync(int entityId)
        {
            var permissionIndex = this.permissions.FindIndex(x => x.Id == entityId);
            if (permissionIndex == -1)
                throw new CustomException(Domain.Constants.ERR_NOT_FOUND, "No se pudo encontrar el registro para ser eliminado. Favor comuniquese con su administrador.");

            return Task.Run(() => this.permissions.RemoveAt(permissionIndex));
        }

        public Task<Local> FindAsync(int id, Expression<Func<Local, object>> include = null)
        {
            var result = this.permissions.Find(x => x.Id == id);

            if (result == null)
                throw new CustomException(Domain.Constants.ERR_NOT_FOUND, $"No se pudo encontrar el registro con el id {id}. Favor comuniquese con su administrador.");

            return Task.FromResult(result);
        }

        public Task<Local> FindAsync(Expression<Func<Local, bool>> expression, Expression<Func<Local, object>> include = null)
        {
            return Task.FromResult(this.permissions.AsQueryable().FirstOrDefault(expression));
        }

        public Task<IQueryable<Local>> FindAllAsync(Expression<Func<Local, object>> include = null)
        {
            return Task.FromResult(this.permissions.AsQueryable());
        }

        public Task<IQueryable<Local>> FindByConditionAsync(Expression<Func<Local, bool>> expression)
        {
            return Task.FromResult(this.permissions.AsQueryable().Where(expression));
        }

        public Task UpdateAsync(Local entity)
        {
            return Task.Run(() =>
             {
                 var permissionIndex = this.permissions.FindIndex(x => x.Id == entity.Id);
                 this.permissions[permissionIndex] = entity;
                 this.permissions[permissionIndex].LastModificationTime = new DateTime();
             });
        }
    }
}
