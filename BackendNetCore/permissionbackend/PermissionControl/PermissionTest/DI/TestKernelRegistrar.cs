﻿using DI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace PermissionTest.DI
{
    public class TestKernelRegistrar : KernelRegistrar
    {
        IServiceCollection _serviceCollection;
        public TestKernelRegistrar(IServiceCollection services)
        {
            this._serviceCollection = services;
        }
        public void RegisterAsDbContext<TContractType, TDbContext>(string contextName) where TDbContext : DbContext, TContractType
        {
            throw new NotImplementedException();
        }

        public void RegisterAsScope(Type contractType, Type implementationType)
        {
            _serviceCollection.AddScoped(contractType, implementationType);
        }

        public void RegisterAsScope<ContractType, Implementation>(string alias = null)
            where ContractType : class
            where Implementation : class, ContractType
        {
            _serviceCollection.AddScoped<ContractType, Implementation>();
        }

        public void RegisterAsSingleton<ContractType, Implementation>(string alias)
            where ContractType : class
            where Implementation : class, ContractType
        {
            _serviceCollection.AddSingleton<ContractType, Implementation>();
        }

        public void RegisterAsSingleton(Type contractType, Type implementationType)
        {
            _serviceCollection.AddSingleton(contractType, implementationType);
        }
        public void RegisterAsSingleton<TServices>(TServices implementationType) where TServices : class
        {
            _serviceCollection.AddSingleton(implementationType);
        }
    }
}
