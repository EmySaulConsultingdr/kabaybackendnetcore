using DI;
using Domain.Data;
using Domain.Enums;
using Domain.Providers;
using Domain.Services;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using PermissionTest.DI;
using Shouldly;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PermissionTest
{
    public class TestService
    {
        private ServiceCollection services { get; set; }
        [SetUp]
        public void Setup()
        {
            this.services = new ServiceCollection();

            Kernel.Instance.TestSetup(new TestKernelRegistrar(services));
        }

        [Test]
        public async Task GET_PERMISSION_ID_3_WHEN_CREATE_NEW_PERMISSION()
        {
            var serviceProvider = this.services.BuildServiceProvider();
            //var permissionRepository = serviceProvider.GetService<LocalService>();

            var permission = new Domain.Models.Local()
            {
              
            };

            //int permissionId = await permissionRepository.CreateAsync(permission);

            //permissionId.ShouldBe(3);
        }

        [Test]
        public void GET_CUSTOM_EXCEPTION_WHEN_PERMISSION_TYPE_IS_10_NOT_VALID()
        {
            var serviceProvider = this.services.BuildServiceProvider();
            //var permissionRepository = serviceProvider.GetService<LocalService>();

            var permission = new Domain.Models.Local()
            {
              
            };

            var ex = Assert.ThrowsAsync<CustomException>(async () =>
            {
                //await permissionRepository.CreateAsync(permission);
            });

            ex.Message.ShouldBe("Tipo de permiso no definido.");
        }

        [Test]
        public async Task GIVE_EXISTING_PERMISSION_UPDATE_EMPLOYEE_NAME_TO_PERMISSION_2()
        {
            var serviceProvider = this.services.BuildServiceProvider();
            //var permissionService = serviceProvider.GetService<LocalService>();
            //var permissionProvider = serviceProvider.GetService<LocalProvider>();

            int id = 2;
            string newName = "Emy Saul";
            string oldName = "Marcos";

            //var permission = await permissionProvider.GetAsync(id);
            //permission.EmployeeName.ShouldBe(oldName);


            //permission.EmployeeName = newName;
            //await permissionService.UpdateAsync(id, permission);

            //var permissionUpdated = await permissionProvider.GetAsync(id);

            //permissionUpdated.EmployeeName.ShouldBe(newName);
        }

        [Test]
        public async Task GIVE_EXISTING_PERMISSION_ID_REMOVE_PERMISSION()
        {
            var serviceProvider = this.services.BuildServiceProvider();
            //var permissionService = serviceProvider.GetService<LocalService>();
            //var permissionProvider = serviceProvider.GetService<LocalProvider>();

            int id = 2;

            //(await permissionProvider.GetAllAsync()).Count().ShouldBe(2);

            //await permissionService.DeleteAsync(id);

            //(await permissionProvider.GetAllAsync()).FirstOrDefault(x => x.Id == 2).ShouldBeNull();
        }

        [Test]
        public void GET_CUSTOM_EXCEPTION_WHEN_NOT_EXISTS_PERMISSION_ID()
        {
            var serviceProvider = this.services.BuildServiceProvider();
            //var permissionService = serviceProvider.GetService<LocalService>();
            //var permissionProvider = serviceProvider.GetService<LocalProvider>();

            int id = -1;

            var ex = Assert.ThrowsAsync<CustomException>(async () =>
            {
                //await permissionService.DeleteAsync(id);
            });

            ex.Message.ShouldBe("No se pudo encontrar el registro para ser eliminado. Favor comuniquese con su administrador.");
        }
    }
}