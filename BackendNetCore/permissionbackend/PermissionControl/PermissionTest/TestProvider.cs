using DI;
using Domain.Data;
using Domain.Enums;
using Domain.Providers;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using PermissionTest.DI;
using Shouldly;
using System.Linq;
using System.Threading.Tasks;

namespace PermissionTest
{
    public class TestProvider
    {
        private ServiceCollection services { get; set; }
        [SetUp]
        public void Setup()
        {
            this.services = new ServiceCollection();

            Kernel.Instance.TestSetup(new TestKernelRegistrar(services));
        }

        [Test]
        public async Task GET_SINGLE_RECORD_WHEN_REQUEST_ALL_PERMISSIONS()
        {
            var serviceProvider = this.services.BuildServiceProvider();

            //var permissionRepository = serviceProvider.GetService<LocalProvider>();
            //var listed = await permissionRepository.GetAllAsync();

            //listed.Count().ShouldBe(2);
            //listed.First().Id.ShouldBe(1);
            //listed.First().EmployeeName.ShouldBe("Emy");
            //listed.First().EmployeeLastname.ShouldBe("Soto");
            //listed.First().PermissionTypeId.ShouldBe((int)StatusRoomEnum.MarriagePaidLeave);
            //listed.First().PermissionType.Id.ShouldBe(3);
            //listed.First().PermissionType.Description.ShouldBe("Permiso retribuido por matrimonio");
        }

        [Test]
        public async Task GET_SECOND_PERMISSION_AND_RETURN_EMPLOYEE_NAME_MARCOS()
        {
            var serviceProvider = this.services.BuildServiceProvider();

            //var permissionRepository = serviceProvider.GetService<LocalProvider>();
            //var permission = await permissionRepository.GetAsync(2);

            //permission.Id.ShouldBe(2);
            //permission.EmployeeName.ShouldBe("Marcos");
            //permission.EmployeeLastname.ShouldBe("Pedro");
            //permission.PermissionTypeId.ShouldBe((int)StatusRoomEnum.SickLeave);
            //permission.PermissionType.Id.ShouldBe(1);
            //permission.PermissionType.Description.ShouldBe("Enfermedad");
        }

        [Test]
        public async Task GET_CUSTOM_EXCEPTION_WHEN_REQUEST_NOT_EXISTING_ID()
        {
            var serviceProvider = this.services.BuildServiceProvider();
            //var permissionRepository = serviceProvider.GetService<LocalProvider>();

            int id = -1;

            //var ex = Assert.ThrowsAsync<CustomException>(async () => await permissionRepository.GetAsync(id));

            //ex.Message.ShouldBe($"No se pudo encontrar el registro con el id {id}. Favor comuniquese con su administrador.");
        }
    }
}