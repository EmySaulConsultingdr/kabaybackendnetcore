﻿using Domain.Data;
using Microsoft.AspNetCore.Http;
using Responses;
using System;
using System.Net;
using System.Threading.Tasks;

namespace CoreBasic.CustomExceptionMiddleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly Domain.Services.LoggerService _logger;

        public ExceptionMiddleware(RequestDelegate next, Domain.Services.LoggerService logger)
        {
            _logger = logger;
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (CustomException ce)
            {
                _logger.LogError($"Error controlado: {ce}");
                await HandleExceptionAsync(httpContext, ce);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error no controlado: {ex}");
                await HandleExceptionAsync(httpContext);
            }
        }

        private Task HandleExceptionAsync(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return context.Response.WriteAsync(new ErrorDetails()
            {
                StatusCode = context.Response.StatusCode,
                Message = "Error interno desde el servidor"
            }.ToString());
        }
        private Task HandleExceptionAsync(HttpContext context, CustomException ce)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = ce.CustomCode == Domain.Constants.ERR_NOT_FOUND ? (int)HttpStatusCode.NotFound : (int)HttpStatusCode.BadRequest;

            return context.Response.WriteAsync(new ErrorDetails()
            {
                StatusCode = context.Response.StatusCode,
                Message = ce.Message
            }.ToString());
        }
    }
}
