﻿using Domain.Providers;
using Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoreBasic.Controllers
{
    [Route("api/category/room/photo")]
    [ApiController]
    public class CategoryRoomPhotoController : CustomBaseController<Domain.Entities.CategoryRoomPhoto, Domain.Models.CategoryRoomPhoto>
    {
        public CategoryRoomPhotoController(BaseProvider<Domain.Entities.CategoryRoomPhoto, Domain.Models.CategoryRoomPhoto> provider,
                                        BaseService<Domain.Entities.CategoryRoomPhoto, Domain.Models.CategoryRoomPhoto> service,
                                        LoggerService logger)
                                        : base(provider, service, logger)
        {
        }

    }
}
