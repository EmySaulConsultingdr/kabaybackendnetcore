﻿using Domain.Providers;
using Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreBasic.Controllers
{
    [Route("api/category/room")]
    [ApiController]
    public class CategoryRoomController : CustomBaseController<Domain.Entities.CategoryRoom, Domain.Models.CategoryRoom>
    {
        private readonly BaseProvider<Domain.Entities.Room, Domain.Models.Room> _roomProvider;
        private readonly BaseProvider<Domain.Entities.CategoryRoomPhoto, Domain.Models.CategoryRoomPhoto> _categoryRoomPhotoProvider;
        private readonly BaseProvider<Domain.Entities.CategoryAttribute, Domain.Models.CategoryAttribute> _categoryAttributeProvider;

        public CategoryRoomController(BaseProvider<Domain.Entities.CategoryRoom, Domain.Models.CategoryRoom> provider,
                                  BaseProvider<Domain.Entities.Room, Domain.Models.Room> roomProvider,
                                  BaseProvider<Domain.Entities.CategoryRoomPhoto, Domain.Models.CategoryRoomPhoto> categoryRoomPhotoProvider,
                                  BaseProvider<Domain.Entities.CategoryAttribute, Domain.Models.CategoryAttribute> categoryAttributeProvider,
                                        BaseService<Domain.Entities.CategoryRoom, Domain.Models.CategoryRoom> service,
                                        LoggerService logger)
                                        : base(provider, service, logger)
        {
            this._categoryRoomPhotoProvider = categoryRoomPhotoProvider;
            this._categoryAttributeProvider = categoryAttributeProvider;
            this._roomProvider = roomProvider;

        }

        [HttpGet("{id}/rooms")]
        public async Task<ActionResult<IEnumerable<Domain.Models.Room>>> GetRoomsByCategory(int id)
        {
            return Ok(await _roomProvider.GetAllAsync(x => x.CategoryRoomId == id));
        }

        [HttpGet("{id}/photos")]
        public async Task<ActionResult<IEnumerable<Domain.Models.CategoryRoomPhoto>>> GetPhotosByCategory(int id)
        {
            return Ok(await _categoryRoomPhotoProvider.GetAllAsync(x => x.CategoryRoomId == id));
        }


        [HttpGet("{id}/attributes")]
        public async Task<ActionResult<IEnumerable<Domain.Models.CategoryAttribute>>> GetAttributesByCategory(int id)
        {
            return Ok(await _categoryAttributeProvider.GetAllAsync(x => x.CategoryRoomId == id));
        }
    }
}
