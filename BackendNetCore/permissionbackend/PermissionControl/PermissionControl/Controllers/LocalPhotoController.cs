﻿using Domain.Providers;
using Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoreBasic.Controllers
{
    [Route("api/local/photo")]
    [ApiController]
    public class LocalPhotoController : CustomBaseController<Domain.Entities.LocalPhoto, Domain.Models.LocalPhoto>
    {
        public LocalPhotoController(BaseProvider<Domain.Entities.LocalPhoto, Domain.Models.LocalPhoto> provider,
                                        BaseService<Domain.Entities.LocalPhoto, Domain.Models.LocalPhoto> service,
                                        LoggerService logger)
                                        : base(provider, service, logger)
        {
        }

    }
}
