﻿using Domain.Providers;
using Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoreBasic.Controllers
{
    [Route("api/category/attribute")]
    [ApiController]
    public class CategoryAttributeController : CustomBaseController<Domain.Entities.CategoryAttribute, Domain.Models.CategoryAttribute>
    {
        public CategoryAttributeController(BaseProvider<Domain.Entities.CategoryAttribute, Domain.Models.CategoryAttribute> provider,
                                        BaseService<Domain.Entities.CategoryAttribute, Domain.Models.CategoryAttribute> service,
                                        LoggerService logger)
                                        : base(provider, service, logger)
        {
        }

    }
}
