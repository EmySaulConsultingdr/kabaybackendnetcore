﻿using Domain.Providers;
using Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreBasic.Controllers
{
    [Route("api/local")]
    [ApiController]
    public class LocalController : CustomBaseController<Domain.Entities.Local, Domain.Models.Local>
    {
        private readonly BaseProvider<Domain.Entities.CategoryRoom, Domain.Models.CategoryRoom> _categoryProvider;
        private readonly BaseProvider<Domain.Entities.LocalPhoto, Domain.Models.LocalPhoto> _localPhotoProvider;
        public LocalController(BaseProvider<Domain.Entities.Local, Domain.Models.Local> provider,
                              BaseProvider<Domain.Entities.CategoryRoom, Domain.Models.CategoryRoom> categoryProvider,
                              BaseProvider<Domain.Entities.LocalPhoto, Domain.Models.LocalPhoto> localPhotoProvider,
                                        BaseService<Domain.Entities.Local, Domain.Models.Local> service,

                                        LoggerService logger)
                                        : base(provider, service, logger)
        {
            this._categoryProvider = categoryProvider;
            this._localPhotoProvider = localPhotoProvider;
        }


        [HttpGet("{id}/categories")]
        public async Task<ActionResult<IEnumerable<Domain.Models.CategoryRoom>>> GetRoomsByLocal(int id)
        {
            return Ok(await _categoryProvider.GetAllAsync(x => x.LocalId == id));
        }

        [HttpGet("{id}/photos")]
        public async Task<ActionResult<IEnumerable<Domain.Models.LocalPhoto>>> GetPhotosByLocal(int id)
        {
            return Ok(await _localPhotoProvider.GetAllAsync(x => x.LocalId == id));
        }
    }
}
