﻿using Domain.Providers;
using Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CoreBasic.Controllers
{
    [Route("api/room")]
    [ApiController]
    public class RoomController : CustomBaseController<Domain.Entities.Room, Domain.Models.Room>
    {
        private readonly BaseService<Domain.Entities.Room, Domain.Models.Room> _service;
        private readonly BaseProvider<Domain.Entities.Room, Domain.Models.Room> _provider;
        public RoomController(BaseProvider<Domain.Entities.Room, Domain.Models.Room> provider,
                                        BaseService<Domain.Entities.Room, Domain.Models.Room> service,
                                        LoggerService logger)
                                        : base(provider, service, logger)
        {
            this._service = service;
            this._provider = provider;
        }

        [HttpPut("{id:int}/status")]
        public async Task<ActionResult> UpdateStatus(int id, Domain.Enums.StatusRoomEnum statusRoom)
        {
            var room = await _provider.GetAsync(id);
            room.StatusRoom = statusRoom;
            await _service.UpdateAsync(id, room);
            return Ok();
        }

    }
}
